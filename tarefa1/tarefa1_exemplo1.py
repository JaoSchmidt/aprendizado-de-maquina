#  Faca um programa que gere o modelo de regressção linear para os dois datasets
# usados nesta aula. Os modelos devem ser gerados de duas formas:
# • Usando as equações apresentadas no slide 15.
# • Usando o modelo de regressção linear disponível no Scikit.

# importar csv, le-lo
# criar array dos valores de x e y
# aplicar equação para angulo1
# aplicar equação para angulo2
# criar grafico

# usando slide 15
import csv

import numpy as np
import matplotlib.pyplot as plt

max_x=-200000
min_x=200000
line_count=0
x_array=[]
y_array=[]
def abrir_csv_analisar_dados(file_name):
    global max_x
    global min_x
    global line_count
    global x_array
    global y_array
    with open(file_name) as csv_file:
        csv_reader=csv.reader(csv_file,delimiter=',')
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
            else:
                x_array.append(float(row[0]))
                y_array.append(float(row[1]))
                if max_x<float(row[0]): max_x=float(row[0])
                if min_x>float(row[0]): min_x=float(row[0])
            line_count += 1

def linear_function_creator():
    soma_x=0
    soma_y=0
    soma_x_y=0
    soma_x2=0
    for i in range(0,len(x_array)):
        soma_x += x_array[i]
        soma_y += y_array[i]
        soma_x_y += x_array[i]*y_array[i]
        soma_x2 += x_array[i]*x_array[i]
    denominador=line_count*soma_x2 - soma_x*soma_x
    x0 = (line_count*soma_x_y - soma_x*soma_y)/denominador
    angulo = (soma_y*soma_x2 - soma_x*soma_x_y)/denominador
    return [angulo,x0]

##################################################
# main
##################################################

abrir_csv_analisar_dados('exemplo1.csv')
result = linear_function_creator()

# result method
print(result[0])
print(result[1])
x = np.array(x_array)
y = result[1]*x + result[0]
y2 = np.array(y_array)



from sklearn.linear_model import LinearRegression

# Use only one feature
X = x.reshape(-1,1)

reg = LinearRegression().fit(X,y_array)
sk_y_pred = reg.predict(X)


# Outputs
plt.subplot(1,2,1)
plt.title("Código Bruto")
plt.plot(x,y,color="green")
plt.scatter(x,y2)

plt.subplot(1,2,2)
plt.title("Usando Sklearn")
plt.scatter(x,y2)
plt.plot(x_array,sk_y_pred,color="red")
plt.show()
