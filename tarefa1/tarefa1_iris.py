#  Faca um programa que gere o modelo de regressção linear para os dois datasets
# usados nesta aula. Os modelos devem ser gerados de duas formas:
# • Usando as equações apresentadas no slide 15.
# • Usando o modelo de regressção linear disponível no Scikit.

# importar csv, le-lo
# criar array dos valores de x e y
# aplicar equação para angulo1
# aplicar equação para angulo2
# criar grafico

# usando slide 15
import csv
import scipy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn.linear_model as sklm


def linear_function_creator(x_array,y_array,line_count):
    soma_x=0
    soma_y=0
    soma_x_y=0
    soma_x2=0
    for i in range(0,len(x_array)):
        soma_x += x_array[i]
        soma_y += y_array[i]
        soma_x_y += x_array[i]*y_array[i]
        soma_x2 += x_array[i]*x_array[i]
    denominador=line_count*soma_x2 - soma_x*soma_x
    angulo = (line_count*soma_x_y - soma_x*soma_y)/denominador
    x0 = (soma_y*soma_x2 - soma_x*soma_x_y)/denominador
    return [angulo,x0]

def calcFTest(x_array,y_array,angulo,x0,line_count):
    sse=0
    ssr=0
    media_y=sum(y_array)/len(y_array)
    print("Media de y = ",media_y)
    for i in range(0,len(x_array)):
        sse += (y_array[i] - (angulo*x_array[i] + x0))**2
        ssr += (media_y - y_array[i])**2
    DFssr = 2
    DFsse = line_count - DFssr
    print("SSE = ",sse)
    print("SSR = ",ssr)
    print("R² = 1-sse/ssr = ",1-sse/ssr)

    F = (ssr*DFsse)/(sse*DFssr)
    alpha = 0.05
    critic_value = scipy.stats.f.ppf(q=1-alpha,dfn=DFssr,dfd=DFsse)

    print("Valor de f = ",F)
    print("Valor crítico = ",critic_value)
    if critic_value < F:
        print("Valor crítico menor que F\nEvidência estatistica pra rejeitar hipótese nula de theta 1 = 0")
    else:
        print("Valor crítico maior que F\nEvidência estatistica pra rejeitar hipótese alternativa de theta 1 != 0")

##################################################
# main
##################################################

# lendo csv

number = "not number"
while not number.isnumeric() or (number.isnumeric() and int(number) != 0):
    while 1:
        print("\nAtributos:\n \
1- petalLengthInCM x petalWidthInCM\n \
2- sepalLengthInCM x sepalWidthInCM\n \
3- sepalLengthInCM x petalLengthInCM\n \
4- sepalLengthInCM x petalWidthInCM\n \
5- sepalWidthInCM x petalLengthInCM\n \
6- sepalWidthInCM x petalWidthInCM\n \
7- sepalLengthInCM x petalLengthInCM REGRESSÃO RIDGE\n \
0- Sair\n")
        number = input("Digite número desejado: ")
        if number.isnumeric() and int(number) >=0 and int(number) <=7:
            break
        else:
            print("Escolha inválida, escolha um número de 0 a 7")

    if int(number) == 0:
        print('exit')
        exit()

    file='iris.csv'
    x_choosen=""
    y_choosen=""
    match(int(number)):
        case 1:
            print("Escolhendo 1")
            x_choosen = "petalLengthInCM"
            y_choosen = "petalWidthInCM"
        case 2:
            print("Escolhendo 2")
            x_choosen = "sepalLengthInCM"
            y_choosen = "sepalWidthInCM"
        case 3:
            print("Escolhendo 3")
            x_choosen = "sepalLengthInCM"
            y_choosen = "petalLengthInCM"
        case 4:
            print("Escolhendo 4")
            x_choosen = "sepalLengthInCM"
            y_choosen = "petalWidthInCM"
        case 5:
            print("Escolhendo 5")
            x_choosen = "sepalWidthInCM"
            y_choosen = "petalLengthInCM"
        case 6:
            print("Escolhendo 6")
            x_choosen = "sepalWidthInCM"
            y_choosen = "petalWidthInCM"
        case 7:
            print("Escolhendo 7")
            x_choosen = "sepalLengthInCM"
            y_choosen = "petalLengthInCM"
        case _:
            print('Comando não reconhecido')

# lendo csv
    df = pd.read_csv(file)

# regressção ridge ###################################################

    if int(number) == 7:
        dfRidge = df.sample(n=10)

        x = dfRidge[[x_choosen]].to_numpy()
        y = dfRidge[[y_choosen]].to_numpy()
        X = x.reshape(-1,1)
        sk_y_pred=[]
        for _lambda in [1.0,5.0,10.0,50.0,100.0]:
            sk_y_pred.append(sklm.Ridge(alpha=_lambda).fit(X,y).predict(X))

        xnormal = df[[x_choosen]].to_numpy()
        ynormal = df[[y_choosen]].to_numpy()

    # Outputs
    ## Código Bruto
        ax = plt.subplot()
        groups = df.groupby('class')
        ax.scatter(xnormal,ynormal,color="grey")
        ax.scatter(x,y,color="red")
        for i in sk_y_pred:
            # extend line
            plt.plot(X,i,color="violet")
        plt.show()

# regressção linear ###################################################
    else:

        x = df[[x_choosen]].to_numpy()
        y2 = df[[y_choosen]].to_numpy()

    # aplicando algoritmo
        result = linear_function_creator(x,y2,len(df.index))
        print("Equação: ",result[0],"* X + ",result[1])

        calcFTest(x,y2,result[0],result[1],len(df.index))

    # result method
        y = result[0]*x + result[1]

    # Use only one feature
        X = x.reshape(-1,1)

        reg = sklm.LinearRegression().fit(X,y2)
        sk_y_pred = reg.predict(X)


    # Outputs
    ## Código Bruto
        plt.subplot(1,2,1)
        plt.title("Código Bruto")
        groups = df.groupby('class')
        for name, group in groups:
            plt.plot(group[x_choosen],group[y_choosen],marker='o',linestyle='None')
        plt.plot(x,y,color="violet")

    ## Usando Skylearn
        plt.subplot(1,2,2)
        plt.title("Usando Sklearn")
        groups = df.groupby('class')
        for name, group in groups:
            plt.plot(group[x_choosen],group[y_choosen],marker='o',linestyle='None')
        plt.plot(x,sk_y_pred,color="violet")

        plt.show()
