# ler o arquivo csv
# retirar as colunas de classificação
# user grubbs test para retirar os outliers
# fazer usuários escolher o target e os atributos
# aplicar equação de regressão linear para multiplos atributos
# gráfico 2D caso selecionado 1 atributos
# gráfico 3D caso selecionado 2 atributos 


import pandas as pd
import numpy as np
import scipy.stats as stats
import statistics


def calculate_critical_value(size, alpha):
    # https://github.com/bhattbhavesh91/outlier-detection-grubbs-test-and-generalized-esd-test-python/blob/master/grubbs-test-for-outliers.ipynb
    t_dist = stats.t.ppf(1 - alpha / (2 * size), size - 2)
    numerator = (size - 1) * np.sqrt(np.square(t_dist))
    denominator = np.sqrt(size) * np.sqrt(size - 2 + np.square(t_dist))
    critical_value = numerator / denominator
    return critical_value

def remove_outliers_from_dataset(dataframe,alpha,silent):
    df = dataframe
    for column_name in dataframe.columns:
        if not silent: print("# Filtrando\'",column_name,"\'##########################")
        while 1:
            column = df[column_name]
            s = sum(column)
            average_y = s/len(column)
            max_y_less_average_y=0
            row=0
            for i in range(0,len(column)):
                if(i in df.index and abs(column[i]-average_y)>max_y_less_average_y):
                    max_y_less_average_y=abs(column[i]-average_y)
                    row=i
            G = max_y_less_average_y/statistics.stdev(column)
            critical_value = calculate_critical_value(len(column),alpha)
            if G > critical_value:
                if not silent: print("G > valor crítico (",G,">",critical_value,"): Outliner na linha ",row," de valor ",column[row])
                df = df.drop(row)
            else:
                if not silent: print("G < valor crítico: Hipotese nula, não existem mais outliners")
                break
    return df

def remove_column_from_dataset(dataframe,column_name):
    dataframe.drop(column_name,axis=1,inplace=True)

##################################################
# main
##################################################

# evita limite no terminal
pd.set_option('display.max_rows', None)

# lê dataframe, ignora index
dataframe = pd.read_csv('data-t1.csv', index_col=False)
remove_column_from_dataset(dataframe,'diagnosis') # remove coluna classificada
remove_column_from_dataset(dataframe,'id') # remove coluna id

# remove outliners
dataframe = remove_outliers_from_dataset(dataframe,0.05,True)

# cria lista de correlação dos diferentes valores
correlation_matrix = dataframe.corr().abs()
correlation_list = correlation_matrix.unstack()
sorted_correlation_list = correlation_list.sort_values().sort_index(level=0,sort_remaining=False,ascending=False)


# com essa lista de correlação, podemos analisar algumas correlações menos imprevisíveis e fazer melhores regressões lineares
#print(correlation_list)


# input do usuário ###############################################################
while 1:
    target = "not number"
    str_input = "not number"
    choosen_variables_array = []
    while 1: # escolha do eixo Y
        choice_target = pd.DataFrame({'Alvo (eixo Y)':sorted_correlation_list['texture_se'].index})
        print(choice_target)
        print('\033[92m' + '30                     Sair' + '\033[0m')
        str_input = input("Escolha o número atributo alvo: ")
        if str_input.isnumeric() and int(str_input) >=0 and int(str_input) < 30:
            target = choice_target['Alvo (eixo Y)'].to_numpy()[int(str_input)]
            print('\'',target,'\' escolhido')
            choice_df = pd.DataFrame({'Variáveis':sorted_correlation_list[target].index,'Correlação':sorted_correlation_list[target].values}).drop(0)
            print("Lista de variáveis\n",choice_df)
            x_axis = input("Escolha as variáveis (e.g.: 1,3,4,6,12,14) ") # escolha do eixo X
            xs = x_axis.split(',')
            if all( item.isnumeric() and int(item) >=1 and int(item) <= 29 for item in xs):
                for x in xs:
                    choosen_variables_array.append(choice_df['Variáveis'].to_numpy()[int(x)-1])
                print(choosen_variables_array)
                break
            else:
                print("Escolha inválida, escolha números entre 0 a 30 em formato de lista e.g.: 9,14,5,20,1,4")
        elif str_input.isnumeric() and int(str_input)==30:
            # permite sair do programa
            print('Saindo')
            exit()
        else:
            print("Escolha inválida, escolha um número de 0 a 30")
    print("Pronto")


    from mpl_toolkits import mplot3d
    import matplotlib.pyplot as plt
    import seaborn as sns
    from sklearn.model_selection import train_test_split
    from sklearn.linear_model import LinearRegression
    from sklearn.metrics import mean_squared_error, mean_absolute_error
    from sklearn import preprocessing

    Y = dataframe[target]
    X = dataframe[choosen_variables_array]

    # regressão linear visto no GFG
    X_train, X_test, y_train, y_test = train_test_split(X,Y, test_size=0.3, random_state=101)
    reg = LinearRegression()
    reg.fit(X_train,y_train)
    c = reg.coef_

    ## Output

    print("Y",end=" = ")
    for i in range(0,len(c)):
        print("[",c[i],"]*x",len(c)-i,end=" + ",sep='')
    print("[",reg.intercept_,"]",sep='')

    ## Output gráfico, caso possível

    xline = X[choosen_variables_array[0]].to_numpy()
    zline = Y.to_numpy()

    if len(X.columns) == 2: # gráfico 3d
        yline = X[choosen_variables_array[1]].to_numpy()

        ax = plt.figure().add_subplot(111,projection='3d')
        ax.scatter3D(xline,yline,zline,color="green")

        xx,yy = np.meshgrid(np.array([min(xline),max(xline)]),np.array([min(yline),max(yline)]))
        Z = c[0]*xx + c[1]*yy + reg.intercept_

        ax.plot_surface(xx,yy,Z,alpha=0.5,)
        plt.title(label=target+": "+choosen_variables_array[0]+" x "+choosen_variables_array[1])
        ax.set_xlabel(choosen_variables_array[0])
        ax.set_ylabel(choosen_variables_array[1])
        ax.set_zlabel(target)
        plt.show()

    elif len(X.columns) == 1: # gráfico 2d
        plt.title(label=target+" x "+choosen_variables_array[0])
        plt.scatter(xline,zline)
        Z = c[0]*xline + reg.intercept_
        plt.plot(xline,Z,color="green")
        plt.xlabel(choosen_variables_array[0])
        plt.ylabel(target)
        plt.show()
