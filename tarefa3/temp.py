import pandas as pd
import numpy as np


x = pd.DataFrame([[1,2],[3,4]])

outputStr = '['+ ','.join(str(s) for s in x.to_numpy()) + '],'

print(outputStr)
