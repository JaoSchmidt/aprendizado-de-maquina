# ler o arquivo csv
# remover linhas com itens nulos

# user grubbs test para retirar os outliers (excluindo colunas de classificação)
# normalizar ou padronizar


# aplicar a regrassão logistica
# aplicar a SVM
# aplicar naive bayes

from os.path import exists
import pandas as pd
import numpy as np
import scipy.stats as stats
import statistics
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
from sklearn import metrics
from sklearn import svm

def calculate_critical_value(size, alpha):
    # https://github.com/bhattbhavesh91/outlier-detection-grubbs-test-and-generalized-esd-test-python/blob/master/grubbs-test-for-outliers.ipynb
    t_dist = stats.t.ppf(1 - alpha / (2 * size), size - 2)
    numerator = (size - 1) * np.sqrt(np.square(t_dist))
    denominator = np.sqrt(size) * np.sqrt(size - 2 + np.square(t_dist))
    critical_value = numerator / denominator
    return critical_value

def remove_outliers_from_dataset(dataframe,columns,alpha):
    df = dataframe
    for column_name in columns:
        while 1:
            column = df[column_name]
            s = sum(column)
            average_y = s/len(column)
            max_y_less_average_y=0
            row=0
            for i in range(0,len(column)):
                if(i in df.index and abs(column[i]-average_y)>max_y_less_average_y):
                    max_y_less_average_y=abs(column[i]-average_y)
                    row=i
            G = max_y_less_average_y/statistics.stdev(column)
            critical_value = calculate_critical_value(len(column),alpha)
            if G > critical_value:
                df = df.drop(row)
            else:
                break
    return df

# não funciona
def estimate_probability(X,coefs,intercepts):
    p = []
    for index, row in X.iterrows():
        thetaT = 0
        for i in range(0,len(row)):
            thetaT += coefs[i]*row[i]
        thetaT += intercepts
        p.append(1/(1+np.exp(-thetaT)))
    return p

def all_or_nothing(p):
    y_final=[]
    for i in p:
        if(i>0.5):
            y_final.append(1)
        else:
            y_final.append(0)
    return y_final

# normatização de dataframe
def normalization(dataframe,_columns):
    return pd.DataFrame(preprocessing.normalize(dataframe, axis=0),columns=_columns)

def standardization(dataframe,_columns):
    return pd.DataFrame(preprocessing.StandardScaler().fit_transform(dataframe),index=dataframe.index,columns=_columns)

def output(DS_y_test,DS_probability,title):
    #print(DS_probability)
    confusion_matrix = metrics.confusion_matrix(DS_y_test,DS_probability)
    precision = confusion_matrix[0][0]/(confusion_matrix[1][0] + confusion_matrix[0][0])
    print("Precisão = ",confusion_matrix[0][0],"/(",confusion_matrix[1][0]," + ",confusion_matrix[0][0],") = ",precision)

    recall = confusion_matrix[0][0]/(confusion_matrix[0][1] + confusion_matrix[0][0])
    print("Recall = ",confusion_matrix[0][0],"/(",confusion_matrix[0][1]," + ",confusion_matrix[0][0],") = ",recall)

    f1 = 2*precision*recall/(precision+recall)
    print("f1 = ",f1)

    print(confusion_matrix)
    #ax = sns.heatmap(confusion_matrix,annot=True)
    #ax.set_title(title)
    #ax.set_ylabel('Resultado real')
    #ax.set_xlabel('Resultado previsto')
    #plt.show()

def categorical_columns(dataframe,columns):
    for col in columns:
        i=1
        for uniques in dataframe[col].unique():
            dataframe[col] = dataframe[col].replace(uniques,i)
            i+=1
    return dataframe

##################################################
# main
##################################################

# evita limite no terminal
pd.set_option('display.max_columns', None)

# lê dataframe, ignora index

# remove valores nulos e outliers
_columns = ['age','fnlwgt','hours-per-week']
if(not exists('train_removed_outliners.csv')):
    DS0 = pd.read_csv('income/train.csv', index_col=False)
    DS0.dropna(inplace=True)
    DS0 = remove_outliers_from_dataset(DS0,_columns,0.05)
    DS0.to_csv('train_removed_outliners.csv',sep=',',index=False)

DS0 = pd.read_csv('train_removed_outliners.csv', index_col=False)
# igual à educational-num
DS0 = DS0.drop('education',axis=1)
# agrupa string de categorias
_categorial_columns=['workclass','marital-status','occupation','relationship','race','gender','native-country']
DS0 = categorical_columns(DS0,_categorial_columns)
DS0 = DS0.drop('capital-loss',axis=1)
DS0 = DS0.drop('capital-gain',axis=1)


# regressão logistica de DS0, usando apenas remoção de outlier por enquanto
DS_train, DS_test, DS_y_train, DS_y_test = train_test_split(DS0.drop('income_>50K',axis=1),DS0['income_>50K'],test_size=0.33, random_state=101)
lreg = LogisticRegression().fit(DS_train,DS_y_train)
DS_probability = lreg.predict(DS_test)
output(DS_y_test,DS_probability,"Regressão logística")

# regressão logistica de DS0, usando remoção de outlier seguido de padronização
DS  = standardization(DS0,_columns)
DS1_train, DS1_test, DS1_y_train, DS1_y_test = train_test_split(DS.drop('income_>50K',axis=1),DS['income_>50K'],test_size=0.33, random_state=101)
lreg = LogisticRegression().fit(DS1_train,DS1_y_train)
DS_probability = lreg.predict(DS1_test)
output(DS1_y_test,DS1_probability,"Regressão logística + padronização")

# regressão logistica de DS0, usando remoção de outlier seguido de normalização
DS  = normalization(DS0,_columns)
DS_train, DS_test, DS_y_train, DS_y_test = train_test_split(DS.drop('income_>50K',axis=1),DS['income_>50K'],test_size=0.33, random_state=101)
lreg = LogisticRegression().fit(DS_train,DS_y_train)
DS_probability = lreg.predict(DS_test)
output(DS_y_test,DS_probability,"Regressão logística + normalização")

# SVM usando remoção de outliner apenas
DS_train, DS_test, DS_y_train, DS_y_test = train_test_split(DS0.drop('income_>50K',axis=1),DS0['income_>50K'],test_size=0.33, random_state=101)
regr = svm.SVC().fit(DS_train,DS_y_train)
DS_probability = regr.predict(DS_test)
output(DS_y_test,DS_probability,"SVM")
